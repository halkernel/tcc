package com.health.naf.model;

import com.health.naf.enums.Restriction;
import com.health.naf.enums.Unity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medicine")
public class Medicine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String commonName;
    private String scientificName;
    private LocalDate validUntil;
    private String weight;
    private Unity unity;
    private Restriction restriction;
    private long quantity;

    private Long batchNumber;
    private LocalDate batchDate;
    private Long dispatched;
}
