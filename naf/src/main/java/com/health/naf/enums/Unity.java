package com.health.naf.enums;

public enum Unity {
    GRAMS("g"), MILLIGRAMS("mg"),  MILLILITER("mL");

    private String unity;

    Unity(String unity) {
        this.unity = unity;
    }

    public String getUnity() {
        return unity;
    }
}
