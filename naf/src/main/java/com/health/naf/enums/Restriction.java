package com.health.naf.enums;

public enum Restriction {
    NO_DIABETICS("Restricted for Diabetic People"),
    ALL_AGES("All Ages Can Use"),
    KIDS_ONLY("Allowed only for Kids");

    private String restriction;

    Restriction(String restriction){
        this.restriction = restriction;
    }

    public String getRestriction() {
        return restriction;
    }
}
