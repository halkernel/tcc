package com.health.naf.dto.request;

import com.health.naf.enums.Restriction;
import com.health.naf.enums.Unity;
import lombok.*;

import java.time.LocalDate;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MedicineRequestTo {

    private String commonName;
    private String scientificName;
    private LocalDate validUntil;
    private String weight;
    private Unity unity;
    private Restriction restriction;
    private long quantity;

}
