package com.health.naf.dto.response;


import lombok.*;

import java.time.LocalDate;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MedicineResponseTo {

    private String commonName;
    private String scientificName;
    private LocalDate validUntil;
    private String weight;
    private String restriction;
    private long quantity;

}
