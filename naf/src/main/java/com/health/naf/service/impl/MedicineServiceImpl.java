package com.health.naf.service.impl;

import com.health.naf.model.Medicine;
import com.health.naf.repository.MedicineRepository;
import com.health.naf.service.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicineServiceImpl implements MedicineService {

    @Autowired
    private MedicineRepository medicineRepository;

    @Override
    public List<Medicine> getAll() {
        return medicineRepository.findAll();
    }

    @Override
    public Medicine getById(Long id) {
        return medicineRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Medicine with (id=%s) not found", id)));
    }

    @Override
    public Medicine create(Medicine medicine) {
        return medicineRepository.save(medicine);
    }

    @Override
    public Medicine dispatch(Long id, Long quantity) {
        Medicine medicine = getById(id);
        medicine.setQuantity(medicine.getQuantity()-quantity);
        return medicineRepository.save(medicine);
    }


}
