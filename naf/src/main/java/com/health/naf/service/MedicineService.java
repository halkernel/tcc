package com.health.naf.service;

import com.health.naf.model.Medicine;

import java.util.List;

public interface MedicineService {

    List<Medicine> getAll();

    Medicine getById(Long id);

    Medicine create(Medicine reservation);

    Medicine dispatch(Long id, Long quantity);

}
