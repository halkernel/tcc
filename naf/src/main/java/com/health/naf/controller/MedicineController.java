package com.health.naf.controller;


import com.health.naf.dto.request.MedicineRequestTo;
import com.health.naf.dto.response.MedicineResponseTo;
import com.health.naf.mapper.MedicineMapper;
import com.health.naf.model.Medicine;
import com.health.naf.service.MedicineService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicines")
public class MedicineController {

    @Autowired
    private MedicineService medicineService;

    @GetMapping("")
    public List<MedicineResponseTo> getAllMedicines(){
        return MedicineMapper.toMedicineResponseToList(medicineService.getAll());
    }

    @GetMapping("/{id}")
    public MedicineResponseTo getMedicineById(@PathVariable Long id){
        return MedicineMapper.toMedicineResponseTo(medicineService.getById(id));
    }

    @PostMapping("")
    public MedicineResponseTo addMedicine(@RequestBody MedicineRequestTo medicineRequestTo){
        Medicine medicine = MedicineMapper.fromMedicineRequestTo(medicineRequestTo);
        Medicine insertedMedicine = medicineService.create(medicine);
        return MedicineMapper.toMedicineResponseTo(medicineService.create(insertedMedicine));
    }

    @PutMapping("/{id}")
    public MedicineResponseTo dispatch(@PathVariable Long id, @RequestParam("quantity") Long quantity){
        return MedicineMapper.toMedicineResponseTo(medicineService.dispatch(id, quantity));
    }

}
