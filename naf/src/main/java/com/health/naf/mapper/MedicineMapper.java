package com.health.naf.mapper;

import com.health.naf.dto.request.MedicineRequestTo;
import com.health.naf.dto.response.MedicineResponseTo;
import com.health.naf.model.Medicine;

import java.util.List;
import java.util.stream.Collectors;

public class MedicineMapper {

    public static MedicineResponseTo toMedicineResponseTo(Medicine medicine){

        return MedicineResponseTo.builder()
                .commonName(medicine.getCommonName())
                .quantity(medicine.getQuantity())
                .scientificName(medicine.getScientificName())
                .validUntil(medicine.getValidUntil())
                .restriction(medicine.getRestriction().getRestriction())
                .weight(medicine.getWeight() + medicine.getUnity().getUnity())
                .build();

    }

    public static Medicine fromMedicineRequestTo(MedicineRequestTo medicineRequestTo){
        return Medicine.builder()
                .commonName(medicineRequestTo.getCommonName())
                .quantity(medicineRequestTo.getQuantity())
                .restriction(medicineRequestTo.getRestriction())
                .scientificName(medicineRequestTo.getScientificName())
                .unity(medicineRequestTo.getUnity())
                .validUntil(medicineRequestTo.getValidUntil())
                .weight(medicineRequestTo.getWeight())
                .build();
    }

    public static List<MedicineResponseTo> toMedicineResponseToList(List<Medicine> medicineList){
        return medicineList.stream().map(MedicineMapper::toMedicineResponseTo).collect(Collectors.toList());
    }
}
