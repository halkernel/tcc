insert into medicine(id, common_name, scientific_name, valid_until, weight, unity, restriction, quantity, batch_number, batch_date, dispatched)
              values(1, 'Luftal', 'Simeticona', '2022-05-04', '75', 2, 2, 60, 1111, '2022-05-04', 0);

insert into medicine(id, common_name, scientific_name, valid_until, weight, unity, restriction, quantity, batch_number, batch_date, dispatched)
              values(2, 'Novalgina', 'Dipirona', '2022-05-04', '500', 1, 2, 600, 1112, '2022-05-04', 0);

insert into medicine(id, common_name, scientific_name, valid_until, weight, unity, restriction, quantity, batch_number, batch_date, dispatched)
              values(3, 'Novalgina', 'Dipirona Monohidratada', '2022-05-04', '100', 0, 2, 460,  1113, '2022-05-04', 0);

insert into medicine(id, common_name, scientific_name, valid_until, weight, unity, restriction, quantity, batch_number, batch_date, dispatched)
              values(4, 'Ibuprofeno', 'Ibuprofeno', '2022-05-04', '250', 1, 2, 240,  1114, '2022-05-04', 0);

insert into user(id, username, password, non_locked, account_non_expired, credential_non_expired, enabled)
              values(1, 'kennet', '$2a$10$nKEKH0qZxjzp5M9YgHsqiemj23GMuMq4P9.nYhZTNzltnQTZ1fgyC', true, true, true, true);

