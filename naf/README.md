#NAF

###Running locally
Requirements:
- Java 11+
- Maven 3.5.X+

Starting service locally:

`mvn spring-boot:run`

###Running on AWS

You can access the endpoints by hitting:

`ec2-52-90-189-177.compute-1.amazonaws.com`


